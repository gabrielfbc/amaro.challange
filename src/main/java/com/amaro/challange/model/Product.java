package com.amaro.challange.model;

import java.io.Serializable;
import java.util.List;

import com.amaro.challange.service.Tag;


@SuppressWarnings("serial")
public class Product implements Serializable
{
	private String id;

	private String name;

	private List<Tag> tags;

	private List<Integer> tagsVector;

	private Double similarity;

	public String getId()
	{
		return this.id;
	}

	public void setId(String id)
	{
		this.id = id;
	}

	public String getName()
	{
		return this.name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public List<Tag> getTags()
	{
		return this.tags;
	}

	public void setTags(List<Tag> tags)
	{
		this.tags = tags;
	}

	public List<Integer> getTagsVector()
	{
		return this.tagsVector;
	}

	public void setTagsVector(List<Integer> tagsVector)
	{
		this.tagsVector = tagsVector;
	}

	public Double getSimilarity()
	{
		return this.similarity;
	}

	public void setSimilarity(Double similarity)
	{
		this.similarity = similarity;
	}

}
