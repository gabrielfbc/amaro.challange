package com.amaro.challange.controller;

import java.nio.file.NoSuchFileException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.amaro.challange.service.ProductList;
import com.amaro.challange.service.ProductService;


@RestController
@RequestMapping(value = "/product")
public class ProductController
{
	@Autowired
	private ProductService productService;

	@RequestMapping(method = RequestMethod.GET)
	public String getMethod()
	{
		return "Available URI's: /getTagsVector and /searchSimilar/{productId}";
	}

	@RequestMapping(value = "/searchSimilar/{productId}", method = RequestMethod.GET)
	public ProductList search(@PathVariable String productId)
	{
		ProductList ret = null;

		try
		{
			ret = getProductService().getSimilarProducts(productId);
		}
		catch (NoSuchFileException e)
		{
			System.out.println("File not found");
		}
		catch (Exception e)
		{
			System.out.println("Error");
		}

		return ret;
	}

	@RequestMapping(value = "/getTagsVector", method = RequestMethod.GET)
	public ProductList getTagsVector(@RequestBody ProductList products)
	{
		try
		{
			getProductService().getTagsVector(products.getProducts());
		}
		catch (Exception e)
		{
			System.out.println("Error");
		}

		return products;
	}

	public ProductService getProductService()
	{
		return this.productService;
	}

	public void setProductService(ProductService productService)
	{
		this.productService = productService;
	}

}
