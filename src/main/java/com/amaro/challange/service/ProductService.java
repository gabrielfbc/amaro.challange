package com.amaro.challange.service;

import java.io.FileNotFoundException;
import java.util.List;

import com.amaro.challange.model.Product;


public interface ProductService
{
	public Product getProductById(String productId) throws Exception;

	public List<Product> getTagsVector(List<Product> products) throws Exception;

	public ProductList getSimilarProducts(String productId) throws Exception, FileNotFoundException;
}
