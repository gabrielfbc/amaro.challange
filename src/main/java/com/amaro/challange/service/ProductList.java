package com.amaro.challange.service;

import java.util.List;
import com.amaro.challange.model.Product;


public class ProductList
{

	private List<Product> products;

	public List<Product> getProducts()
	{
		return this.products;
	}

	public void setProducts(List<Product> products)
	{
		this.products = products;
	}

}
