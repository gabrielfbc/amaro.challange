package com.amaro.challange.service;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.amaro.challange.model.Product;
import com.fasterxml.jackson.databind.ObjectMapper;


@Service
public class ProductServiceImpl implements ProductService
{

	private ProductList productList;

	@Override
	public ProductList getSimilarProducts(String productId) throws Exception, FileNotFoundException
	{
		ProductList ret = new ProductList();

		final Product product = getProductById(productId);

		if (product != null)
		{
			ret.setProducts(searchSimilarProducts(product));
		}

		return ret;
	}

	@Override
	public Product getProductById(String productId) throws Exception, FileNotFoundException
	{
		Product ret = null;
		final ProductList productListObj = getProductList();

		if (productListObj != null)
		{
			final List<Product> products = productListObj.getProducts();
			if (products != null)
			{
				//get the first element with productId
				List<Product> resultProductList = products.stream()
						.filter(product -> product.getId() != null && product.getId().equals(productId)).collect(Collectors.toList());
				if (resultProductList != null && !resultProductList.isEmpty())
				{
					ret = resultProductList.get(0);
				}
			}
		}

		return ret;
	}

	@Override
	public List<Product> getTagsVector(List<Product> products) throws Exception
	{
		if (products != null)
		{
			final List<Tag> tags = Arrays.asList(Tag.values());

			//create the tags vector and set it to the product
			products.forEach(product -> {
				final List<Integer> tagsVector = createTagsVector(product, tags);
				product.setTagsVector(tagsVector);
			});
		}

		return products;
	}

	private List<Integer> createTagsVector(Product product, final List<Tag> tags)
	{
		final List<Tag> productTags = product.getTags();
		List<Integer> tagsVector = new ArrayList<Integer>();

		if (productTags != null)
		{
			tags.forEach(tag -> {
				Integer contains = productTags.contains(tag) ? 1 : 0;
				tagsVector.add(contains);
			});
		}

		return tagsVector;
	}

	private ProductList getProductList() throws Exception, FileNotFoundException
	{
		if (this.productList == null)
		{
			ObjectMapper mapper = new ObjectMapper();
			String json = new String(Files.readAllBytes(Paths.get("src/main/resources/productsWithTagsVector.txt")));
			this.productList = mapper.readValue(json, ProductList.class);
		}

		return this.productList;
	}

	private List<Product> searchSimilarProducts(Product product) throws FileNotFoundException, Exception
	{
		List<Product> ret = null;
		final ProductList productListObj = getProductList();

		if (productListObj != null)
		{
			final List<Product> products = productListObj.getProducts();

			if (products != null)
			{
				final String productId = product.getId();
				final List<Integer> productTagsVector = product.getTagsVector();

				if (productId != null && productTagsVector != null)
				{
					products.forEach(p -> {
						final String id = p.getId();
						final List<Integer> tagsVector = p.getTagsVector();

						if (id != null && tagsVector != null)
						{
							Double similarity = calculateSimilarity(productTagsVector, tagsVector);
							p.setSimilarity(similarity);
						}
					});

					sortProductsBySimilarity(products);

					//get the three most similar products
					ret = products.stream().filter(p -> !p.getId().equals(productId)).limit(3).collect(Collectors.toList());
				}
			}
		}

		return ret;
	}

	private Double calculateSimilarity(final List<Integer> v1, final List<Integer> v2)
	{
		Double ret = null;

		if (v1 != null && v2 != null)
		{
			Double sum = 0d;
			for (int i = 0; i < v1.size(); i++)
			{
				Integer num = v1.get(i) - v2.get(i);
				sum += Math.pow(num.doubleValue(), 2d);
			}
			Double d = Math.sqrt(sum);
			Double s = 1 / (1 + d);
			ret = roundDouble(s);
		}

		return ret;
	}

	private double roundDouble(Double value)
	{
		long roundedInt = Math.round(value * 100);
		double result = (double) roundedInt / 100;

		return result;
	}

	private void sortProductsBySimilarity(List<Product> products)
	{
		products.sort((Product p1, Product p2) -> p2.getSimilarity().compareTo(p1.getSimilarity()));
	}
}
