package com.amaro.challange;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.assertj.core.api.Assert;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.amaro.challange.model.Product;
import com.amaro.challange.service.ProductList;
import com.amaro.challange.service.ProductService;
import com.amaro.challange.service.Tag;


@SpringBootTest
class ChallangeApplicationTests
{

	@Autowired
	private ProductService productService;

	@Test
	void contextLoads()
	{
	}

	@Test
	public void createTagsVector() throws Exception
	{
		// given
		List<Product> products = new ArrayList<Product>();
		Product p1 = new Product();

		p1.setId("8371");
		p1.setName("VESTIDO TRICOT CHEVRON");

		List<Tag> tags = new ArrayList<Tag>();
		tags.add(Tag.balada);
		tags.add(Tag.neutro);
		tags.add(Tag.delicado);
		tags.add(Tag.festa);
		p1.setTags(tags);

		products.add(p1);

		// when
		getProductService().getTagsVector(products);

		// then
		List<Integer> tagsVector = Arrays.asList(1, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0);
		Assertions.assertEquals(tagsVector, products.get(0).getTagsVector());
	}

	@Test
	public void searchSimilarProducts() throws Exception
	{
		// given
		final String productId = "8371";
		
		// when
		final ProductList productList = getProductService().getSimilarProducts(productId);
		final List<Product> products = productList.getProducts();
		
		// then
		Assertions.assertEquals(3, products.size());
		Assertions.assertEquals(0.31, products.get(0).getSimilarity());
		Assertions.assertEquals(0.31, products.get(1).getSimilarity());
		Assertions.assertEquals(0.29, products.get(2).getSimilarity());
	}

	public ProductService getProductService()
	{
		return this.productService;
	}

	public void setProductService(ProductService productService)
	{
		this.productService = productService;
	}
	
}
